package simpleex.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Pair;
import simpleex.core.LatLong;
import simpleex.core.MetaData;
import simpleex.ui.tags.TagsBar;

/**
 * Controller class for the location metadata editor.
 */
public class MetaDataEditorController {
  /**
   * Generic metadata event.
   */
  public static final EventType<MetaDataEvent> OPTIONS_ALL = new EventType<>("OPTIONS_ALL");

  /**
   * Specific metadata event dispatched when the metadata is saved.
   */
  public static final EventType<MetaDataEvent> METADATA_SAVED = new EventType<>(
      OPTIONS_ALL, "METADATA_SAVED"
    );

  private LatLong latlong;

  @FXML
  private BorderPane rootContainer;

  @FXML
  private VBox centerVBox;

  @FXML
  private Button saveButton;

  @FXML
  private Button cancelButton;

  @FXML
  private TextField nameInput;

  @FXML
  private TextArea descriptionInput;

  @FXML
  private TableView<Pair<String, String>> propertiesTableView;

  @FXML
  private TableColumn<Pair<String, String>, String> propertyNamesColumn;

  @FXML
  private TableColumn<Pair<String, String>, String> propertyValuesColumn;

  @FXML
  private TextField newValueInput;

  @FXML
  private TextField newKeyInput;

  @FXML
  private Button buttonAddUpdate;

  @FXML
  private Button buttonDelete;

  @FXML
  private TagsBar tagsBar;

  @FXML
  private Label coordinatesLabel;

  @FXML
  private void initialize() {
    propertyNamesColumn.setCellValueFactory(
        cellData -> new SimpleStringProperty(
          ((Pair<String, String>) (cellData.getValue())).getKey())
    );

    propertyValuesColumn.setCellValueFactory(
        cellData -> new SimpleStringProperty(
          ((Pair<String, String>) (cellData.getValue())).getValue())
    );
    propertiesTableView.getSelectionModel().selectedItemProperty()
        .addListener(new ChangeListener<Pair<String, String>>() {

          @Override
          public void changed(
              ObservableValue<? extends Pair<String, String>> observable, 
              Pair<String, String> oldValue,
              Pair<String, String> newValue) {
            if (newValue == null) {
              buttonAddUpdate.setDisable(false);
              buttonDelete.setDisable(true);
            } else {
              buttonAddUpdate.setDisable(false);
              buttonDelete.setDisable(false);
              newKeyInput.setText(newValue.getKey());
              newValueInput.setText(newValue.getValue());
            }
          }
        });
  }

  @FXML
  void onCancel(ActionEvent event) {
    closeDialog(event);
  }

  @FXML
  void onLocationName(ActionEvent event) {

  }

  void closeDialog(ActionEvent event) {
    Stage stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
    stage.close();
  }

  @FXML
  void onSave(ActionEvent event) {
    latlong.getMetaData().setProperty(MetaData.NAME_PROPERTY, nameInput.getText());
    latlong.getMetaData().setProperty(MetaData.DESCRIPTION_PROPERTY, descriptionInput.getText());

    ObservableList<Pair<String, String>> props = propertiesTableView.getItems();
    final Iterator<String> propertyNames = latlong.getMetaData().propertyNames();
    while (propertyNames.hasNext()) {
      final String propName = propertyNames.next();
      if (MetaData.STD_PROPERTIES.contains(propName)) {
        continue;
      } else {
        propertyNames.remove();
      }
    }
    for (Pair<String, String> pair : props) {
      latlong.getMetaData().setProperty(pair.getKey(), pair.getValue());
    }

    final ObservableList<Node> tags = tagsBar.getTags();
    latlong.getMetaData().setTags();
    for (Node node : tags) {
      latlong.getMetaData().addTags(((TagsBar.Tag) node).getTag());
    }

    Event.fireEvent(
        ((Button) event.getSource()).getScene().getWindow(), 
        new MetaDataEvent(METADATA_SAVED)
    );
    closeDialog(event);
  }

  void setLatLong(LatLong latLong) {
    this.latlong = latLong;
    updateUi();
  }

  private void updateUi() {
    nameInput.setText(latlong.getMetaData().getProperty(MetaData.NAME_PROPERTY));
    descriptionInput.setText(latlong.getMetaData().getProperty(MetaData.DESCRIPTION_PROPERTY));
    coordinatesLabel.setText(latlong.toString());

    updatePropertiesTable();
    updateTags();
  }

  /**
   * Update handler.
   *
   * @param event event object
   */
  @FXML
  public void onAddUpdateProperty(ActionEvent event) {
    final String newKey = newKeyInput.getText();
    final String newValue = newValueInput.getText();
    if (!newKey.isBlank() && (MetaData.isCustomProperty(newKey))) {
      latlong.getMetaData().setProperty(newKey, newValue);
      updatePropertiesTable();
    }
  }

  private void updatePropertiesTable() {
    MetaData metaData = latlong.getMetaData();
    propertiesTableView.getItems().clear();
    final Iterator<String> propertyNames = metaData.propertyNames();
    Collection<Pair<String, String>> locationProperties = new ArrayList<Pair<String, String>>();
    if (propertyNames.hasNext()) {
      while (propertyNames.hasNext()) {
        final String propertyName = propertyNames.next();
        if (MetaData.STD_PROPERTIES.contains(propertyName)) {
          continue;
        } else {
          Pair<String, String> p = new Pair<String, String>(propertyName, 
              metaData.getProperty(propertyName));
          locationProperties.add(p);
        }
      }
      propertiesTableView.getItems().addAll(locationProperties);
    }
  }

  private void updateTags() {
    tagsBar.clearAllTags();
    MetaData metaData = latlong.getMetaData();
    final Iterator<String> tags = metaData.tags();
    while (tags.hasNext()) {
      final String tag = tags.next();
      tagsBar.addTag(tag);
    }
  }

  @FXML
  public void onDeleteProperty(ActionEvent event) {
    int selectedIndex = propertiesTableView.getSelectionModel().getSelectedIndex();
    propertiesTableView.getItems().remove(selectedIndex);
  }
}
