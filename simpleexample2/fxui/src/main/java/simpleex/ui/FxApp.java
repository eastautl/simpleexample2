package simpleex.ui;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import simpleex.core.LatLong;
import simpleex.core.LatLongs;
import simpleex.core.MetaData;

public class FxApp extends Application {

  // private HttpServer restServer = null;
  @Override
  public void start(final Stage stage) throws Exception {
    URI baseUri = null;
    final List<String> args = getParameters().getRaw();
    if (args.size() >= 1) {
      final List<String> serverArgs = new ArrayList<String>();
      baseUri = URI.create(args.get(0));
      serverArgs.add(baseUri.toString());
      if (args.size() >= 2) {
        // json of initial data
        serverArgs.add(args.get(1));
      }
      System.out.println(serverArgs);
      // restServer = LatLongGrizzlyApp.startServer(serverArgs.toArray(new String[serverArgs.size()]), 5);
    }
    final String fxml = (baseUri != null ? "FxAppUsingRest.fxml" : "FxApp.fxml");
    final FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(fxml));
    final Parent root = fxmlLoader.load();
    if (baseUri == null) {
      // set initial data manually
      final FxAppController controller = fxmlLoader.getController();
      // controller.setLatLongs(new LatLongs(63.1, 11.2, 63.2, 11.0));
      controller.setLatLongs(getInitialData());
    } else {
      // final FxAppUsingRestController controller = fxmlLoader.getController();
      // controller.setDataAccess(new RestLatLongsDataAccess(baseUri +
      // LatLongsService.LAT_LONG_SERVICE_PATH, controller.getObjectMapper()));
    }
    final Scene scene = new Scene(root);
    stage.setScene(scene);
    stage.show();
  }

  @Override
  public void stop() throws Exception {
    // if (restServer != null) {
    // restServer.shutdown();
    // }
    super.stop();
  }

  /**
   * Launches the app.
   *
   * @param args the command line arguments
   */
  public static void main(final String[] args) {
    // only needed on ios
    System.setProperty("os.target", "ios");
    System.setProperty("os.name", "iOS");
    System.setProperty("glass.platform", "ios");
    System.setProperty("targetos.name", "iOS");
    launch(args);
  }

  /**
   * Method to prepare the initial entries in the loaction list.
   *
   * @return LatLongs instance with several items
   */
  protected LatLongs getInitialData() {
    final LatLongs latLongs = new LatLongs();
    latLongs.addLatLong(new LatLong(63.1, 11.2));
    latLongs.addLatLong(new LatLong(63.2, 11.0));
    final LatLong latLongWithMetaData = new LatLong(63.5, 11.5);
    latLongWithMetaData.getMetaData().setProperty(MetaData.NAME_PROPERTY, "Awsome place");
    latLongWithMetaData.getMetaData().setProperty(MetaData.DESCRIPTION_PROPERTY,
        "Lorem ipsum dolor sit amet,"
            + " consectetur adipiscing elit. Nulla placerat urna non aliquet imperdiet. Nullam tincidunt felis "
            + "vel sem blandit viverra. Etiam non volutpat erat. In hac habitasse platea dictumst. In lacus quam, "
            + "rutrum vel malesuada non, molestie eu velit. Donec ut vulputate tortor, id convallis enim. Mauris "
            + "et ipsum volutpat, dictum risus sed, aliquet sapien. Nam congue fermentum porta. Nullam non "
            + "odio consequat, laoreet est eget, egestas dui. Aliquam suscipit elit non nisi sagittis, nec "
            + "ultrices leo condimentum. Maecenas vel ligula nec mi feugiat volutpat. Aenean semper nisi sed"
            + " tortor maximus tristique. Vestibulum at mauris massa. Nulla laoreet, velit eu lobortis efficitur, "
            + "tortor sem molestie massa, at pellentesque tortor elit a nibh. In vel orci vitae magna rhoncus pulvinar "
            + "sit amet id erat.");
    latLongWithMetaData.getMetaData().addTags("tag 1", "tag 2", "a much longer tag 3");
    latLongWithMetaData.getMetaData().setProperty("custom property 1", "this is the value for custom property 1");
    latLongWithMetaData.getMetaData().setIntegerProperty("custom property 2 (int)", 13);
    latLongWithMetaData.getMetaData().setDoubleProperty("custom property 3 (double)", 35.13);
    latLongWithMetaData.getMetaData().setBooleanProperty("custom property 4 (boolean)", false);
    latLongs.addLatLong(latLongWithMetaData);
    return latLongs;
  }

}
