package simpleex.restapi;

import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import simpleex.core.LatLong;
import simpleex.core.LatLongs;

@Path(LatLongsService.LAT_LONG_SERVICE_PATH)
public class LatLongsService {

  public static final String LAT_LONG_SERVICE_PATH = "latLongs";

  @Inject
  private LatLongs latLongs;

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public LatLongs getLatLongs() {
    return latLongs;
  }

  /**
   * Returns the LatLong at the provided position.
   *
   * @param num the position
   */
  @GET
  @Path("/{num}")
  @Produces(MediaType.APPLICATION_JSON)
  public LatLong getLatLong(@PathParam("num") int num) {
    if (num < 0) {
      num = latLongs.getLatLongCount() + num;
    }
    return latLongs.getLatLong(num);
  }

  /**
   * Adds the LatLongs.
   *
   * @param latLongs the LatLongs to add
   */
  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public int addLatLongs(final List<LatLong> latLongs) {
    return this.latLongs.addLatLongs(latLongs.toArray(new LatLong[latLongs.size()]));
  }

  /**
   * Sets the LatLong at the provided position.
   *
   * @param latLong the new latLong
   * @param num the posision to set
   */
  @PUT
  @Path("/{num}")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public int setLatLong(final LatLong latLong, @PathParam("num") int num) {
    if (num < 0) {
      num = latLongs.getLatLongCount() + num;
    }
    latLongs.setLatLong(num, latLong);
    return num;
  }

  /**
   * Removes the LatLong at the provided position.
   *
   * @param num the position of the LatLong that will be removed
   */
  @DELETE
  @Path("/{num}")
  @Produces(MediaType.APPLICATION_JSON)
  public LatLong deleteLatLong(@PathParam("num") int num) {
    if (num < 0) {
      num = latLongs.getLatLongCount() + num;
    }
    return latLongs.removeLatLong(num);
  }
}

/*
 * @startuml
 * client -> LatLongsService: GET /latLongs
 * LatLongsService --> client: [ ]
 * client -> LatLongsService: POST /latLongs ~[[63.1, 11.2], [63.2, 11.0]]
 * LatLongsService --> client: 0
 * client -> LatLongsService: GET /latLongs/1
 * LatLongsService --> client: { "latitude": 63.2, "longitude": 11.0 }
 * client -> LatLongsService: PUT /latLongs/0 { "latitude": 64.0, "longitude": 9.1 }
 * LatLongsService --> client: 0
 * client -> LatLongsService: GET /latLongs
 * LatLongsService --> client: [ { "latitude": 64.0, "longitude": 9.1 }, { "latitude": 63.2, "longitude": 11.0 } ]
 * @enduml
 */
