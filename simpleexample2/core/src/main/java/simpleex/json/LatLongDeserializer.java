package simpleex.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import simpleex.core.LatLong;
import simpleex.core.MetaData;

/**
 * JSON serializer for LatLong.
 */
public class LatLongDeserializer extends JsonDeserializer<LatLong> {

  private static int ARRAY_JSON_NODE_SIZE = 2;

  @Override
  public LatLong deserialize(JsonParser jsonParser, DeserializationContext deserContext)
      throws IOException, JsonProcessingException {
    JsonNode jsonNode = jsonParser.getCodec().readTree(jsonParser);
    return deserialize(jsonNode);
  }

  /**
   * Decodes the provided JsonNode to a LatLong object.
   *
   * @param jsonNode the JsonNode to decode
   * @return the corresponding LatLong object
   */
  public LatLong deserialize(JsonNode jsonNode) {
    if (jsonNode instanceof ObjectNode) {
      ObjectNode objectNode = (ObjectNode) jsonNode;
      double latitude = objectNode.get(LatLongSerializer.LATITUDE_FIELD_NAME).asDouble();
      double longitude = objectNode.get(LatLongSerializer.LONGITUDE_FIELD_NAME).asDouble();
      LatLong latLon = new LatLong(latitude, longitude);
      if (objectNode.has(LatLongSerializer.META_DATA_FIELD_NAME)) {
        ObjectNode metaDataNode = (ObjectNode) objectNode.get(
            LatLongSerializer.META_DATA_FIELD_NAME
          );
        MetaData metaData = latLon.getMetaData();
        if (metaDataNode.has(LatLongSerializer.TAGS_FIELD_NAME)) {
          for (JsonNode tagNode : (ArrayNode) metaDataNode.get(LatLongSerializer.TAGS_FIELD_NAME)) {
            metaData.addTags(tagNode.asText());
          }
        }
        if (metaDataNode.has(LatLongSerializer.PROPERTIES_FIELD_NAME)) {
          for (JsonNode propertyNode : (ArrayNode) metaDataNode.get(LatLongSerializer.PROPERTIES_FIELD_NAME)) {
            String propertyName = (propertyNode instanceof ArrayNode ? ((ArrayNode) propertyNode).get(0)
                : ((ObjectNode) propertyNode).get(LatLongSerializer.PROPERTIES_NAME_FIELD_NAME)).asText();
            String propertyValue = (propertyNode instanceof ArrayNode ? ((ArrayNode) propertyNode).get(1)
                : ((ObjectNode) propertyNode).get(LatLongSerializer.PROPERTIES_VALUE_FIELD_NAME)).asText();
            metaData.setProperty(propertyName, propertyValue);
          }
        }
      }
      return latLon;
    } else if (jsonNode instanceof ArrayNode) {
      ArrayNode locationArray = (ArrayNode) jsonNode;
      if (locationArray.size() == ARRAY_JSON_NODE_SIZE) {
        double latitude = locationArray.get(0).asDouble();
        double longitude = locationArray.get(1).asDouble();
        return new LatLong(latitude, longitude);
      }
    }
    return null;
  }
}
